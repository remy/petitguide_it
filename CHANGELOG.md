
   - Update 04/2020 Covid-19 : je n'avais pas fait d'update depuis Nov. 2019, il s'agit donc d'une grosse mise à jour.

        - Framasoft ne veut pas se substituer à des solutions qui devraient être déployées par des ministères (éducation, enseignement supérieur).
        - Une liste d'outils et de services pour les situations de télétravail : https://pads.tedomum.net/p/T%C3%A9l%C3%A9travail_et_les_outils_libres
        - les sections visios, tchat, VPN et dans une moindre mesure agenda et prises de notes ont été mises à jour.

  - Update 01/2021 : une fois de plus, quelques mois sont passés. On notera l'annonce préoccupante de WhatsApp sur le [changement de ses conditions d'utilisation](https://www.lavoixdunord.fr/917612/article/2021-01-07/whatsapp-impose-de-nouvelles-conditions-d-utilisation-permettant-de-partager), [les déboires de Qwant](https://www.developpez.com/actu/268567/Qwant-enquete-sur-les-deboires-du-Google-francais-hauts-salaires-deficit-subventions-utilisation-de-Bing-et-Bing-Ads/), les débats sur la censure et la liberté d'expression (Twitter, loi Avia (voir [1](https://www.france24.com/fr/info-en-continu/20210109-le-bannissement-de-trump-de-twitter-pose-des-questions-de-r%C3%A9gulation-estime-c%C3%A9dric-o), [2](https://blogs.mediapart.fr/laurent-chemla/blog/100121/la-chute-2), [3](https://fr.m.wikipedia.org/wiki/Loi_contre_les_contenus_haineux_sur_internet))) et la cybersurveillance ([Drones contre Quadrature du net](https://www.liberation.fr/france/2020/12/22/le-conseil-d-etat-interdit-d-utiliser-des-drones-pour-filmer-les-manifestations-a-paris_1809419), [débats sur StopCovid puis TousAntiCodid](https://risques-tracage.fr/), le débat autour du [Health Data Hub et Microsoft](https://www.numerama.com/politique/632223-confier-le-health-data-hub-a-microsoft-le-peril-pour-les-donnees-de-sante-ne-serait-pas-prouve.html), les débats autour de Zoom...[1](https://protonmail.com/blog/zoom-privacy-issues/), [2](https://theintercept.com/2020/11/14/zoom-censorship-leila-khaled-palestine/), [3](https://www.telegraph.co.uk/technology/2020/06/11/zoom-locked-activists-account-marking-tiananmen-square-anniversary/), [4](https://www.techrepublic.com/article/who-has-banned-zoom-google-nasa-and-more/), [5](https://onezero.medium.com/zoom-is-a-nightmare-so-why-is-everyone-still-using-it-1b05a4efd5cc),[6](https://www.bleepingcomputer.com/news/security/over-500-000-zoom-accounts-sold-on-hacker-forums-the-dark-web/), [7](https://en.wikipedia.org/wiki/Zoombombing), [8](https://www.bbc.com/news/technology-52200507), [9](https://www.theregister.com/2020/04/01/zoom_spotlight/) etc).

       - ainsi Qwant a été supprimé des moteurs recommandés ([nombreux déboires](https://www.bienpublic.com/france-monde/2019/09/23/pour-tout-comprendre-des-polemiques-autour-de-qwant)),
       - Firefox Send s'est arrêté [en septembre 2020](https://support.mozilla.org/fr/kb/qu-est-il-arrive-firefox-send) et donc n'est plus mentionné.

  - Update 04/2021 : rajout de nombreux logiciels pour smartphone (`Shelter`, `nebulo`, `Tasks.org`, `Aurora Store`, ...). Qwant s'est beaucoup amélioré dernièrement et est revenu dans les moteurs conseillés.
       
       - weboob devient woob suite à des polémiques,
       - ajout de nebulo comme alternative à Blokada DNS,
       - ajout de Shelter,
       - ajout d'une description de fdroid,
       - ajout de Aurora Store,
       - ajout de Tasks.org,
       - changement de moteur de recherche PeerTube (bascule vers le moteur officiel).

