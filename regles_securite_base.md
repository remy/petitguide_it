# Quelques aspects de sécurité informatique à l'usage des utilisateurs

<small>version `0.0.1`, le 25/09/2019. Auteur : Dmitri Montviloff dmitri.montviloff _at_ umontpellier.fr</small>
<!-- to generate the html version: `pandoc --ascii -f markdown -t html regles_securite_base.md -o regles_securite_base.html` -->

## Introduction

Une des responsabilités des utilisateurs d'un Système d'Information (SI) est de ne pas compromettre la sécurité dudit SI : en témoignent toutes les chartes informatiques / réglements intérieurs dont la raison d'être est justement de sensibiliser tout un chacun sur ce point.
La sécurité informatique est très souvent perçue comme étant du ressort des informaticiens : cela impliquerait que la sécurité n'est qu'une question technique, ce qui est faux...
En réalité, que ce soit dans le *cyberespace* ou dans le *meatspace* (ie. : la 'vraie vie'), la sécurité est l'affaire de tous, mais surtout une question de **comportement**. 
Lors de la mise en place d'une PSSI d'une Université, un consultant avait l'habitude de répéter :
**La sécurité, c'est 80 % utilisateurs, 15 % procédures et 5 % technique**

Un autre façon (rigolote) de le dire :
*Le maillon faible de la sécurité informatique, c'est l'interface chaise-clavier*

Faire de la sécurité informatique va donc en grande partie consister à identifier les comportements potentiellement dangereux et à réduire (faire disparaître) ces comportements : c'est l'objet de ce guide.

### A qui s'adresse ce guide

Ce guide s'adresse à tous les usagers d'un Système d'Information (informaticiens ou pas) afin de les sensibiliser à la sécurité informatique, c'est à dire à faire prendre conscience à tout un chacun des risques liés à certaines habitudes parfois très répandues chez les usagers des SI.
L'expression "usagers des SI" recouvre toute personne se connectant, par quelque moyen que ce soit (ordinateur, tablette, smartphone, etc.), au réseau d'une organisation et utilisant ses ressources informatiques. On pourrait étendre cette notion notamment aux logiciels communiquant depuis (/vers) l'extérieur avec le SI mais, dans un soucis de simplification, on va (pour l'instant) s'en tenir aux humains :-) ...
Remarque importante : ce qui suit vaut quel que soit votre système d'exploitation ; l'immunité prétendue de certains systèmes d'exploitation aux problèmes de sécurité (en particulier les virus) est un mythe.

## Des mauvais usages en informatique

Petite revue, non exhaustive et non ordonnée, de mauvaises habitudes que vous devez perdre ;-p ...

  - **Utiliser un compte admin**,
  - **Se balader avec un portable non chiffré**,
  - **Utiliser un mot de passe (nul) pour toutes les applications**,
  - **Donner son mot de passe à quelqu'un, le mettre sur un post-it ou un cahier,**,
  - **Ne pas faire de sauvegardes**,
  - **Utiliser les services des [GAFAM](https://fr.wikipedia.org/wiki/GAFAM)**,
  - **Ne pas prendre soin de son identité numérique**,
  - **Penser que si on est sous Linux / Mac, on est immunisé aux virus et autres bêbêtes**,
  - **Installer des logiciels qui ne viennent pas du site de l'éditeur**,
  - **Ne pas faire participer (au moins demander conseil à) un informaticien compétent à un projet impliquant de l'informatique**,
  - **Ne pas signaler les incidents de sécurité informatique (vol de machine/données, virus , etc. )**,
  - **Ouvrir des pièces jointes non sollicitées/ clés USB trouvées**,
  - **Ne pas faire les mises à jour de son système**,
  - **Ne pas séparer les usages (pro/perso)**,
  - **Enregistrer mes mots de passe / numéros de CB sur mon ordi/smartphone/tablette (sans application dédiées)**,
  - **Ne pas vérifier qu'on surfe en HTTPS ou qu'on se trouve bien sur un site légitime**,
  - **Penser que la princesse nigérianne va effectivement verser quelques millions sur votre compte**.

## Références

### ANSSI
  - [Guide d'hygiène informatique de l'ANSSI](https://www.ssi.gouv.fr/uploads/2017/01/guide_hygiene_informatique_anssi.pdf)


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.