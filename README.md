# Petit guide personnel pour une informatique plus responsable

# Financièrement, écologiquement, interopérable, libre, et respectueuse de votre vie privée

<small>version `0.1.2`, le 02/04/2021. Auteur : Rémy Dernat remy.dernat _at_ umontpellier.fr</small>
<!-- to generate the html version: `pandoc --ascii --toc -f markdown -t html README.md -o README.html` -->

[Petit guide personnel pour une informatique plus responsable](#petit-guide-personnel-pour-une-informatique-plus-responsable)
- [Financièrement, écologiquement, interopérable, libre, et respectueuse de votre vie privée](#financièrement-écologiquement-interopérable-libre-et-respectueuse-de-votre-vie-privée)
    - [Introduction](#introduction)
        - [A qui s'adresse ce guide](#a-qui-sadresse-ce-guide)- [Ce que ce petit guide ne couvre pas](#ce-que-ce-petit-guide-ne-couvre-pas)
        - [A propos de la consommation des applications](#a-propos-de-la-consommation-des-applications)
        - [A propos des applications libres](#a-propos-des-applications-libres)
    - [Logiciels et services](#logiciels-et-services)
        - [Le navigateur](#le-navigateur)
        - [Moteur de recherche](#moteur-de-recherche)
        - [Flux d'informations, recherche bibliographique](#flux-dinformations-recherche-bibliographique)
        - [Réseaux Sociaux](#réseaux-sociaux)
            - [Facebook](#facebook)
        - [Appels videos, Videoconferences et Webconferences](#appels-videos-videoconferences-et-webconferences)
            - [A propos des salons, chat](#a-propos-des-salons-chat)
            - [SMS, MMS chiffres](#sms-mms-chiffres)
        - [Prise de note](#prise-de-note)
        - [Cartes](#cartes)
        - [Agenda](#agenda)
        - [Espaces de partage](#espaces-de-partage)
        - [Creation de formulaires](#creation-de-formulaires)- [Mise en place de reunions, meetings](#mise-en-place-de-reunions-meetings)
            - [Organisation d'évènements](#organisation-dévènements)
        - [Contenu multimedia](#contenu-multimedia)
            - [Lecture](#lecture)
            - [Creation et edition de contenu multimedia](#creation-et-edition-de-contenu-multimedia)
            - [Creation de posters](#creation-de-posters)- [Partage](#partage)
        - [Mails](#mails)
            - [Mails privés](#mails-privés)
            - [Client de messagerie](#client-de-messagerie)
            - [Messagerie chiffree](#messagerie-chiffree)
            - [VPN](#vpn)
                - [Tor](#tor)
        - [Enregistrement et partage de publications](#enregistrement-et-partage-de-publications)
        - [Redaction d'articles scientifiques](#redaction-darticles-scientifiques)
        - [Suites bureautiques](#suites-bureautiques)
            - [Presentations, slides](#presentations-slides)
            - [Suites bureautiques en ligne, collaboratives](#suites-bureautiques-en-ligne-collaboratives)
        - [Reprenez le contrôle de votre vie privée](#reprenez-le-contrôle-de-votre-vie-privée)
- [Pointeurs, bibliographie](#pointeurs-bibliographie)

## Introduction

Dans un cadre toujours plus contraint en ressources, il me paraît essentiel de faire un point sur l'état de l'art des pratiques informatiques. Je ne prétends pas tout savoir sur la question, bien au contraire, mais j'explore tout cela depuis assez longtemps, de part mes activités professionnelles, dans un institut tourné vers l'environnement et l'écologie ([INEE, CNRS](https://inee.cnrs.fr/)). Le [CNRS](http://www.cnrs.fr) a par ailleurs déjà pas mal d'expertise dans le domaine; sans parler du côté recherche, le groupe [ecoinfo](https://ecoinfo.cnrs.fr/) publie régulièrement [des informations](https://ecoinfo.cnrs.fr/2015/01/22/communications/) sur l'emprunte écologique du numérique. De plus, le [projet plume](https://www.projet-plume.org/ressource/plume-historique-du-projet-et-de-la-plate-forme-depuis-aout-2013), dont le but était de **Promouvoir les Logiciels Utiles Maîtrisés et Économiques dans l'Enseignement Supérieur et la Recherche** a permis de recenser et comparer de nombreux logiciels (plus de 400) entre 2006 et 2013. Repris sous le nom FENIX (Fiches d'Évaluation Normalisées Issues de l'eXpérience) en 2016, le projet semble être pour l'heure à l'arrêt (https://www.projet-plume.org/ressource/fenix-historique-du-projet-et-de-la-plate-forme-depuis-2016).

L'informatique n'est pas du tout écologique, bien au contraire. Certains discours laissent à penser que l'informatique serait _magique_, avec un impact environnemental quasi nul, avec des programmes de plus en plus sur le _cloud_... Il y a de nombreux serveurs (malgré des termes tel que _serverless_) réunis dans des datacentres qui consomment de grosses quantités d'énergie (cf. [1](https://www.connaissancedesenergies.org/sites/default/files/pdf-pt-vue/ufe_data_centers_0.pdf)), reliés entre eux par des réseaux, qui eux aussi, consomment.

> **Note importante**
>
>   - Je pense qu'une grande partie des problèmes peut se résoudre avec une informatique "libre", mais ce n'est pas toujours le cas, car parfois l'alternative libre n'existe pas ou n'est simplement pas _suffisamment_ adaptée. Par ailleurs, la logique _écologique_ est encore rarement prise en compte (autant pour les solutions propriétaires que libres).
>   - Le guide évolue régulièrement; un logiciel conseillé à un instant T, peut ne plus l'être à T+1 (_ainsi, certains service recommandés de type [`framasoft`](https://framasoft.org/en/) (c'est à dire ceux qui commencent par frama\*), peuvent être dépréciés ([lire le communiqué "deframasoftisons"](https://framablog.org/2019/09/24/deframasoftisons-internet/)) pour être progressivement remplacés par des services tels que ceux proposés par les [`CHATONS`](https://chatons.org/find)_). Je vous conseille de lire le [changelog](CHANGELOG.md) pour avoir les informations de changements de version.

### A qui s'adresse ce guide

Il s'adresse principalement aux utilisateurs de ressources informatiques, en particulier pour des scientifiques, mais reste dans l'ensemble généraliste. Néanmoins, un professionnel de l'informatique peut également y piocher quelques idées.

### Ce que ce petit guide ne couvre pas

Vous ne trouverez pas d'informations sur le matériel informatique (qui peut être OpenSource (on parle d'_Open Hardware_), et respectueux de l'environnement (voir par ex. [Fair Phone](https://www.fairphone.com), [label TCO](http://www.ecolabelindex.com/ecolabel/tco-development), [ROHS](https://www.tme.eu/fr/en/information/about-company/6637/rohs/)...)) ou sur les systèmes d'exploitation (bien qu'étant linuxien convaincu).

Vous ne trouverez pas non plus d'informations sur des logiciels spécifiques à telle ou telle branche d'activité (par ex. je ne parle pas des IDEs ou des outils de gestion de projet). En effet, ce guide se veut généraliste.

### A propos de la consommation des applications

D'un point de vue énergétique, les cartes graphiques, les disques durs et le CPU sont les éléments qui consomment le plus sur une machine. Donc plus les applications font appels à ces composants, plus l'empreinte énergétique est forte. Pour le disque dur, ça dépend de la fréquence et des accès qui sont faits (IOs), mais même non utilisés, allumés, tous ces composants consomment de l'énergie.

Ainsi, les applications qui font appel massivement au CPU, comme l'encodage vidéo, à la carte graphique, comme les jeux vidéos, ou le _[mining](https://fr.wikipedia.org/wiki/Minage_de_cryptomonnaie)_ des cryptomonnaies, devraient être utilisées avec parcimonie.

Il faut aussi se méfier des applications qui font beaucoup appel au réseau et utilisent beaucoup de bande passante, car ça signifie que d'autres machines effectuent peut-être des traitements, éventuellement très coûteux en énergie, et ça fait fonctionner d'autant plus les équipements réseau.

Par ailleurs, il vaut mieux éviter, de manière générale les applications lourdes "usines à gaz" qui font plein de choses et dont nous utilisons uniquement 5% des fonctionnalités. L'approche plus écologique étant d'utiliser l'application qui remplit uniquement le besoin requis, de la manière la plus minimaliste possible. 

Ainsi, certains informaticiens utilisent des applications telles que `mutt` (client mail dans un terminal), `irc` (messagerie instantanée) ou encore `vi`, `emacs`. Le but de ce guide n'est pas de parler de ce type d'application, même si elles sont plus éco-responsables que de nombreuses applications plus modernes.

Dans la suite de ce guide, j'ai souvent pris le parti de présenter des solutions qui fonctionnent sur la majorité des systèmes d'exploitation. Ce choix qui simplifie la lecture et le choix pour l'utilisateur n'est cependant pas un choix écologique. Pour être la plus performante et la plus efficiente possible, une application doit être écrite au maximum dans un langage "bas niveau" (proche du processeur; mais ça ne veut pas dire qu'une application écrite dans un langage haut niveau ne serait pas efficace...). Ce que je sous-entends ici, c'est que pour fonctionner sur une majorité de systèmes d'exploitation, l'application doit être écrite dans un langage qui s'exécute dans une sorte de machine virtuelle (ex: Java avec JVM) ou embarquer suffisamment de choses pour que le système soit capable de la comprendre. Ce n'est donc pas un choix écologique, et, de manière générale, éviter les application en Java ou ElectronJS/Nodejs est une bonne chose. Mais parfois, il en va également de l'_expérience utilisateur_ ([UX](https://fr.wikipedia.org/wiki/Exp%C3%A9rience_utilisateur)), qui fera que vous adopterez ou non l'application.

### A propos des applications libres

De même les applications _libres_ sont souvent décriées pour ce "manque" de confort utilisateur. Toutefois, vous verrez que, si c'est parfois vrai pour de nouvelles petites applications, ce n'est plus du tout vrai pour les projets plus anciens et plus importants.

Enfin, l'approche consumériste dans le numérique qui permet aux éditeurs de logiciels propriétaires de vendre régulièrement le produit par une obsolescence programmée (fin de la licence, du support, ou par le biais des mises à jour rendant inutilisable le logiciel), n'est pas une solution. Devoir de nouveau acheter des licences, n'est pas une fatalité. Le libre nous aide dans cette approche en nous permettant d'accroître nos compétences, à reprendre une autonomie, une liberté de décision et être maître des coûts. C'est donc le chemin vers une informatique plus durable et mieux maîtrisée.

La possibilité de pouvoir auditer le code permet également aux personnes intéressées/compétentes de s'assurer de ce que fait le logiciel et que ce dernier est sûr, fiable, sécurisé.

## Logiciels et services

Ces derniers peuvent être hors ligne ou au contraire, en ligne.

### Le navigateur

Il me semble opportun de commencer ce petit guide par cette thématique. En effet, l'informatique moderne est de moins en moins « locale » et de plus en plus tournée vers Internet, dont le Web en particulier, avec des sites et des logiciels qui deviennent des WebApps et un business de plus en plus en ligne (Amazon, etc...)...

[`Mozilla Firefox`](https://www.mozilla.org/fr/firefox/) est le plus respectueux des standards du Web, de la vie privée et de la consommation de votre machine. Les autres navigateurs connus sont tous intimement liés à une marque, à l'exception de [`Chromium`](https://www.chromium.org/) (la version libre de [`Google Chrome`](https://www.google.com/chrome/)) et [`Opera`](https://www.opera.com) (https://www.lesnumeriques.com/appli-logiciel/navigateur-web-chrome-firefox-edge-ou-opera-qu-est-ce-qui-les-distingue-a138171.html).

D'un point de vue de la vitesse de chargement, `Google Chrome` était souvent mis en avant, mais ce n'est plus vrai depuis quelques versions de `Firefox` (cf. [article 2017 Firefox quantum et chrome sur "laptopmag"](https://www.laptopmag.com/articles/firefox-quantum-vs-chrome), ou [article mai 2019](https://gizmodo.com/new-firefox-competes-with-chrome-on-speed-and-privacy-1834921185)). Il faudra cependant faire attention aux nombre d'onglets ouverts, aux nombres d'extensions et aux sites visités.

Si, comme moi, vous faites le choix de Firefox, certaines extensions peuvent vous être utiles :

- [`Privacy Badger`](https://www.eff.org/privacybadger) (bloque les traceurs sur les sites visités)
- [`Adblock Plus`](https://adblockplus.org) (bloqueur de pub)
- [`uBlock Origin`](https://github.com/gorhill/uBlock) (bloque des éléments tiers pisteurs/publicités)
- [`HTTPS Everywhere`](https://www.eff.org/fr/https-everywhere) (permet de basculer automatiquement vers la version HTTPS d'un site lorsqu'elle existe).
- [`mailvelope`](https://www.mailvelope.com) (permet de chiffrer vos mails pour la majorité des fournisseurs de _"webmail"_)

Il y en a beaucoup d'autres (dont [`Ghostery`](https://www.ghostery.com) qui bloque aussi publicités et traceurs), qui peuvent aussi vous être utiles, selon vos habitudes de navigation. Attention à ne pas confrondre l'extension « _Adblock_ » avec « _Adblock Plus_ ». Cependant, plus vous rajoutez d'extensions, plus vous aurez la sensation que `Firefox` devient _lent_ (_sauf, bien évidemment, ceux qui empêchent le chargement de code ou de données parasites supplémentaires et optimisent par ce biais, l'utilisation de la bande passante_)...

[`Firefox ESR`](https://www.mozilla.org/en-US/firefox/organizations/all/), permet d'avoir une version avec un support étendu. Pour le personnel du CNRS, c'est le navigateur recommandé pour accéder à l'application `Sirhus`.

### Moteur de recherche

Ce service est ligne, qui s'exécute sur une multitude de serveurs, est souvent (pour ne pas dire "tout le temps"), le point d'entrée vers le Web.

Il y a une citation très répandue et qui se vérifie très souvent en informatique et dans d'autres domaines : *« If it is free, you are the product »*. Cette réflexion qui amène à réfléchir sur « pourquoi c'est gratuit », est souvent une question relative aux conditions d'utilisation d'un service, les fameuses [CGU](https://fr.wikipedia.org/wiki/Conditions_g%C3%A9n%C3%A9rales_d%27utilisation). Je ne vous apprendrai sûrement rien du scandale de [Cambridge Analytica / Facebook](https://fr.wikipedia.org/wiki/Scandale_Facebook-Cambridge_Analytica). Ces exemples pleuvent sur le Web régulièrement.

Du point de vue du respect de la vie privée, les moteurs de recherche à préférer selon moi sont :

- [https://duckduckgo.com](https://duckduckgo.com/),
- [https://www.ecosia.org](https://www.ecosia.org/),
- [https://www.lilo.org](https://www.lilo.org/),
- [https://www.qwant.com](https://www.qwant.com).

`duckduckgo` est le plus connu des 3.

`duckduckgo` est en fait un proxy vers les principaux moteur de recherche (`Google` essentiellement), donc vous aurez les mêmes résultats qu'avec ces derniers. Il permet donc de protéger votre vie privée. `duckduckgo` permet d'utiliser des [`bangs!`](https://duckduckgo.com/bang) pour lancer des recherches sur des sites tiers (ex: [`!ptube libreoffice tutorial`](https://duckduckgo.com/?q=!ptube+libreoffice+tutorial)). A noter que `duckduckgo` est américain, il peut donc être touché par des lois américaines telles que le [PatriotAct](https://fr.wikipedia.org/wiki/USA_PATRIOT_Act).

`Qwant`, est un moteur de recherche français qui a son propre index (ce n'est pas un meta moteur de recherche comme les 3 autres cités), mais dont les calculs tournent sur des serveurs du cloud de Microsoft (_Azure_). Les résultats sont d'ailleurs parfois un peu similaires à *Bing*, malgré un algorithme différent. Cependant, vous pouvez rajouter des _« flags »_ à la fin de votre recherche si les résultats ne vous semblent pas pertinents. Ainsi, `&g` à la fin de votre recherche, renverra vers *Google*.

`Ecosia` permet de planter des arbres lorsqu'on fait des recherches. Il se dit « respectueux de la vie privée », tout comme les 3 autres. Les résultats sont issus principalement du moteur de recherche de Microsoft, *Bing* ainsi que *Yahoo !*.

`Lilo` est un meta moteur de recherche français qui combine les résultats de *Bing*, *Google* et *Yahoo !*. Son fonctionnement est un peu similaire à `ecosia` mais il vous permet de choisir quelle bonne cause aider.

Les alternatives OpenSource, en terme de moteur de recherche, ne me semblent pas suffisamment abouties pour être recommandées à ce stade (_searx_, _yaci_, _gigablast_...)

> Note : une initiative intéressante est l'extension firefox [`meta-press.es`](https://www.meta-press.es/fr/) qui permet de faire des recherches directement dans des journaux sans passer par un moteur de recherche.

### Flux d'informations, recherche bibliographique

Ici, malheureusement, je n'aurai sûrement pas tous les pointeurs, mais une réelle concurrence à *[Google Scholar](https://scholar.google.fr/)* serait saine. Je pense qu'il est préférable d'utiliser [Web Of Science](https://www.webofknowledge.com) et d'être abonné aux flux RSS de _([bio](https://www.biorxiv.org/))[arXiv](https://arxiv.org/)_, _[PeerCommunityIn](https://peercommunityin.org/)_ et _[HAL](https://hal.archives-ouvertes.fr/)_, mais les données devront être agrégées. Je reste ouvert à toute information complémentaire à ce sujet. A noter l'existence de [`scinapse`](https://scinapse.io), qui semble plus honnête que *google scholar* dans leurs [CGU](https://scinapse.io/terms-of-service).

_Voir aussi : [Sci-Hub](https://fr.wikipedia.org/wiki/Sci-Hub)_

### Réseaux Sociaux

[`Twitter`](https://twitter.com/) est désormais une référence dans le monde scientifique. Il existe une réelle alternative, décentralisée, non liée à une quelconque entreprise ou gouvernement. C'est [`Mastodon`](https://fr.wikipedia.org/wiki/Mastodon_%28r%C3%A9seau_social%29). Il s'agit d'un réseau ouvert, décentralisé et fédéré, composé de nombreuses instances (serveurs). Il est bien entendu possible de dialoguer entre personnes sur différentes instances (réseau fédéré : voir le principe du [`fediverse`](https://fr.wikipedia.org/wiki/Fediverse)). De prime abord, pour des habitués de `twitter`, ce réseau s'avère déconcertant, difficile à appréhender. Lorsque vous aurez pris vos marques, vous préférerez l'atmosphère plus calme, généralement moins polémique de `Mastodon`.

Je suis conscient qu'on reste principalement sur un réseau pour les contacts, cependant, si vous y allez, vous entraînerez peut-être des personnes avec vous !

Pour ces réseaux, vous pouvez utiliser [Forget](https://forget.codl.fr/about/), qui s'occupera de faire le ménage sur votre compte à intervals réguliers !

#### Facebook

Il me parait difficile de faire l'impasse sur ce réseau social, mais si vous avez un compte dessus, je vous conseille de fuir au plus vite (voir par exemple [cet article sur _wired_](https://www.wired.com/story/facebook-scandals-2018/)) et de désinstaller les applications associées (_facebook_, _messenger_, _whatsapp_, _Instagram_).

En alternative, je vous conseille d'utiliser [`Diaspora*`](https://diasporafoundation.org/). Pour [`Instagram`](https://www.instagram.com), vous pouvez utiliser [`pixelfed`](https://pixelfed.org/). Pour les 2 autres, voir ci-dessous.

### Appels videos, Videoconferences et Webconferences

Rappelons que si vous n'avez pas besoin de la visio, vous pouvez utiliser un serveur audio comme [mumble](https://www.mumble.com/) ou uniquement du tchat. Cette solution sera plus écologique.

Pour la visio [`Skype`](https://www.skype.com) est malheureusement la référence, mais de nombreuses alternatives bien plus respectueuses de votre vie privée existent. Ainsi, dans l'enseignement supérieur / recherche, préférez l'utilisation de [`rendez-vous`](https://rendez-vous.renater.fr/home/) de _[Renater](https://www.renater.fr/)_, basé sur [`jitsi meet`](https://jitsi.org/jitsi-meet/). Cet outil, très facile d'utilisation permet de créer un « salon » qui est un espace Web temporaire. Seul l'organisateur a besoin d'un compte lié à l'enseignement supérieur/recherche. Tout se fait depuis un simple navigateur (partage d'écran, de document, chat, son/vidéo...).

Si vous avez une conférence avec plus de 10 intervenants, il vaudra mieux basculer sur des solutions telles que [Renavisio](https://renavisio.renater.fr/), compatible avec les protocoles SIP, et qu'il est possible de rejoindre avec un simple téléphone.

`rendez-vous` est basé sur la solution libre [`Jitsi Meet`](https://jitsi.org/jitsi-meet/). Dans la même veine, on peut citer [`framatalk`](https://framatalk.org/accueil/fr/). D'autres alternatives libres existent : https://opensource.com/alternatives/skype

Sur smartphone, les solutions dominantes sont principalement [`Whatsapp`](https://www.whatsapp.com) et [`Facetime`](https://apps.apple.com/us/app/facetime/id1110145091). Je ne saurai que trop vous conseiller de passer sur [`Signal`](https://www.signal.org/). Ce logiciel OpenSource permet de passer des appels chiffrés et n'est pas associé à un géant de l'informatique. Il est aussi conseillé par _[Edward Snowden](https://fr.wikipedia.org/wiki/Edward_Snowden)_, un lanceur l'alerte et défenseur de la vie privée bien connu.

Je n'ai pas évalué [Telegram](https://telegram.org/), mais signalons que le gouvernement français a choisi de développer sa propre solution nommée [Tchap](http://www.tchap.fr/), basée sur [`Riot/Matrix`](https://about.riot.im/) (pour le moment réservée aux agents de l'état français) pour éviter d'utiliser Telegram (pour cause de soupçons d'espionnage possibles... ?).
`Telegram`, tout comme `Signal` sont tous deux des logiciels OpenSource (_note : Malheureusement, `Signal` est centralisé, et la gestion des clés n'est pas évidente. Je ne prétends donc pas que cette solution soit parfaite (pour les personnes intéressées, une matrice de comparaison [ici](https://www.securemessagingapps.com/) ou plus complète [ici](https://securechatguide.org/featuresmatrix.html)), mais c'est la plus facile à utiliser et permet de s'assurer d'un minimum de sécurité_).

> Update 04/2020 Covid-19 : suite à la crise d _covid-19_, j'ai été amené à évoluer bien d'autres solutions. On a eu le boom de lax solution `Zoom` (accompagné du _ZoomBombing_ et de [toutes autres joyeusetés](https://www.nextinpact.com/news/108877-la-direction-interministerielle-numerique-deconseille-fortement-application-zoom.htm)). Les solutions à base de `Jitsi meet` sont intéressantes [1], mais ne tiennent pas vraiment la route au-delà de 5/10 personnes. Il vaut mieux utiliser `chromium` pour pouvoir se connecter à un salon. Enfin, l'application [`BigBlueButton`](https://bigbluebutton.org/) semble très intéressante, car ele permet d'accueillir beaucoup plus de mondeque `jitsi`. Par ailleurs, [`Nextcloud Talk`](https://nextcloud.com/talk/) m'a aussi donné satisfaction (bien qu'ayant les mêmes difficultés que `jitsi` pour tenir au-delà de a poignée de personnes), tout comme [`Jami`](https://jami.net/), un petit logiciel multi-plateforme en P2P à portée de tous. Je préfère d'ailleurs désormais `Jami` à `Signal` quand mon correspondant a un compte/le logiciel.

[1] [Interview du créateur de jitsi sur la webradio de l'April](https://media.april.org/audio/radio-cause-commune/libre-a-vous/emissions/20200407/libre-a-vous-20200407-interview-emil-ivov-createur-jitsi.ogg)

#### A propos des salons, chat

Le but du paragraphe précédent n'était pas de présenter des alternatives de messagerie directe de type [`Slack`](https://slack.com) (bien que les outils précédents puissent remplir cet objectif avantageusement). Les conversations y sont orientées en salon thématiques, et l'inscription se fait par mail. Si c'est le type d'outil que vous cherchez, voici [une liste](https://opensource.com/alternatives/slack) qui vous sera utile. Vous pourrez alors vous tourner, par exemple, vers [Riot](https://riot.im/) ou simplement [IRC](https://fr.wikipedia.org/wiki/Internet_Relay_Chat) (bien plus basique, sans visio/vidéo).

> Update 04/2020 Covid-19 : là encore, j'ai pu tester de nombreuses solutions. Les solutions OpenSource sont à favoriser : [`Mattermost`](https://mattermost.com/), [`RocketChat`](https://rocket.chat/), et `Matrix/Riot`. Ces derniers proposent également des plugins pour `jitsi` voir `BigBlueButton` (au minimum pour `Mattermost` et `RocketChat`).

#### SMS, MMS chiffres

Si vous ne souhaitez pas passer par `Signal` pour gérer cette partie, vous pouvez toujours utiliser [`Silence`](https://github.com/SilenceIM/Silence) qui chiffrera le contenu de vos SMS/MMS.

### Prise de note

Vous utilisez peut-être [`Evernote`](https://evernote.com), [`MS OneNote`](https://www.onenote.com), [`Google Keep`](https://keep.google.com), [`notion.so`](https://www.notion.so). En alternative (moins poussées que `notion.so`), je pense que [`Joplin`](https://joplinapp.org/), [`carnet`](https://getcarnet.app/), [`Simplenote`](https://simplenote.com/) ou [`Zim`](https://zim-wiki.org/) sauront sûrement vous combler. Pour vous guider dans votre choix, il faudra vérifier si vous pouvez vous synchroniser aux autres outils que vous utilisez déjà, voir si vous pouvez travailler hors ligne et si l'outil vous convient dans sa prise en main.

Dans un cadre plus poussé, l'utilisation de [`Zettlr`](https://www.zettlr.com/) peut également être une bonne idée, grâce à des fonctionnalités pour [faire des citations](https://docs.zettlr.com/en/academic/citations/) ou exporter en [`Latex`](https://fr.wikipedia.org/wiki/LaTeX) ou en PDF.

Personnellement, j'utilise désormais [`Nextcloud Notes`](https://apps.nextcloud.com/apps/notes). C'est la solution la plus pratique si vous avez un serveur `Nextcloud` à disposition. Mais, en local, `Zettlr` permet d'avoir un éditeur plus poussé.

A noter que [`pandoc`](https://pandoc.org/) permet de convertir des notes en markdown dans le format que vous souhaitez.

### Cartes

[`Google Map`](https://www.google.fr/maps) fait ici figure de référence. [`OpenStreetMap`](https://www.openstreetmap.org) est un projet communautaire libre d'utilisation et OpenSource. Sous `Android`, vous pouvez l'utiliser avec l'application [`osmand`](https://osmand.net/), [`MAPS.ME`](https://maps.me/) ou bien avec [`qwant maps`](https://www.qwant.com/maps). Si `osmand` est très bien pour des randonneurs, il vaut mieux privilégier les 2 autres pour des itinéraires en voiture. C'est 3 applications sont OpenSource. Cependant, [`Exodus privacy`](#exodus) n'est pas tendre avec `MAPS.ME`, avec 17 pisteurs (dont `Facebook`, `Google`, `Twitter`...) et 16 autorisations listés. Je laisse toutefois ce logiciel dans cette liste, en attendant que `qwant maps` soit plus finalisé.
Je déconseille vivement l'utilisation de [`waze`](https://www.waze.com) vis à vis de l'énergie consommée par l'application (de plus, c'est aussi une application qui repose sur `Google` / `Google Map`). De même, je vous conseille de désactiver le GPS lorsque cette fonction ne vous est pas indispensable.

### Agenda

Généralement, les messageries professionnelles, de type _[groupware](https://fr.wikipedia.org/wiki/Groupware)_, comme [`Zimbra`](https://www.zimbra.com) ou [`BlueMind`](https://www.bluemind.net), embarquent un calendrier partagé ([`CalDAV`](https://fr.wikipedia.org/wiki/CalDAV)). [`Nextcloud`](https://nextcloud.com) permet également d'en utiliser un. Si vous n'avez pas accès à ce type de messagerie, mais que vous souhaitez utiliser un agenda partagé ou que vous préférez en utiliser un différent pour votre usage personnel (il faudra alors certainement synchroniser votre agenda professionnel et votre agenda privé avec [`Etar`](https://github.com/Etar-Group/Etar-Calendar/) par ex., combiné avec [`DAVx5`](https://www.davx5.com/) pour la synchronisation), vous pouvez vous tourner vers [`Framagenda`](https://framagenda.org/login) ou d'autres messageries privées qui proposent ce type de service.

### Espaces de partage

Les références propriétaires dans le domaine sont [`Google Drive`](https://drive.google.com), [`Dropbox`](https://www.dropbox.com), [`WeTransfer`](https://wetransfer.com/) ou encore [`MEGA`](https://mega.nz/). L'espace en ligne n'est jamais gratuit, ou fortement limité, car personnel ou associatif. Le protocole [`P2P`](http://fr.wikipedia.org/wiki/Pair_%C3%A0_pair), avec notamment [`bittorent`](http://fr.wikipedia.org/wiki/BitTorrent) permettent de répondre à cette problématique. Bien que très performant, il faut alors faire confiance à "ses pairs".

Si votre institution met à disposition un serveur (S)FTP, [`seafile`](https://www.seafile.com) ou [`nextcloud`](https://nextcloud.com), ça sera certainement la meilleure alternative possible (au `CNRS`, nous disposons par exemple de [`mycore`](https://www.ods.cnrs.fr/my_core.php) basé sur nextcloud, et [`filesender`](https://filesender.renater.fr/) fourni par [`Renater`](https://www.renater.fr/)).

Sinon, il reste la possibilité de faire soit-même son serveur de partage (avec [`yunohost`](https://yunohost.org/#/) ou [`lufi`](https://demo.lufi.io/) par exemple) ou utiliser [`framadrop`](https://framadrop.org/).

Lufi dispose d'une [extension Firefox](https://addons.mozilla.org/fr/firefox/addon/lufi-webextension/) pour partager des documents sur une instance Lufi.

Pour partager rapidement des données entre 2 personnes sur Internet, vous pouvez aussi utiliser [`croc`](https://github.com/schollz/croc) qui n'utilisera pas de serveur tiers.

### Creation de formulaires

[`Framaforms`](https://framaforms.org/) est une alternative libre à [`Google Form`](https://docs.google.com/forms).

### Mise en place de reunions, meetings

[`doodle`](https://doodle.com/) est la référence, mais il existe des alternatives sans publicité : chez `Renater`, nous avons [`evento`](https://evento.renater.fr/) pour le personnel de l'_[ESR](http://www.enseignementsup-recherche.gouv.fr/)_ (seul celui qui crée l'évènement doit obligatoirement faire partie d'un organisme affilié). Il existe également [`framadate`](https://framadate.org/) de chez `framasoft`.

#### Organisation d'évènements

La référence actuelle reste [`MeetUp`](https://www.meetup.com) et [`Facebook`](https://www.facebook.com/). Une alternative est en train de se créer, [`mobilizon`](https://joinmobilizon.org/fr/).

### Contenu multimedia

#### Lecture

Hors ligne ou en ligne [`VLC`](https://www.videolan.org/vlc/) est la référence, et cocorico, c'est français.

Pour lire des vidéos [`Youtube`](https://www.youtube.com), sans passer par `Youtube`, vous pouvez utiliser [`invidio.us`](https://www.invidio.us/) qui vous permet de chercher et lire des vidéos `Youtube` sans la publicité et le pistage de `Google` ou l'application [`FreeTube`](https://github.com/FreeTubeApp/FreeTube/releases). [`youtube-dl`](https://github.com/ytdl-org/youtube-dl/blob/master/README.md#readme) est un petit programme en ligne de commande (en `python`) vous permettant de chercher et télécharger des vidéos en provenance de `Youtube` et d'autres sites de partage de vidéos comme [`Dailymotion`](https://www.dailymotion.com) [\*](#href_outoftheweb).

Sur smartphone, si vous avez installez [F-droid](https://www.f-droid.org) sur votre smartphone, vous pouvez aussi utiliser l'application [`NewPipe`](https://newpipe.schabi.org/) qui permet de lire des vidéos `Youtube`, sans les pisteurs et la publicité habituelle.

<a id="href_outoftheweb">_A propos : Web sans navigateur_</a>

A noter qu'il y a de nombreux projets pour utiliser le Web sans un navigateur classique, tel que [`woob`](https://woob.tech/) ou [`kresus`](https://kresus.org/) pour gérer vos comptes en banque (lui-même basé sur `woob`).

#### Creation et edition de contenu multimedia

Je ne vous conseille pas d'utiliser les logiciels `Adobe`, mais j'avoue ne pas être un professionnel de contenu multimedia, et je comprends que certains ne puissent pas s'en passer. En revanche, je vous conseille toutefois de faire le [tour des alternatives OpenSource](https://itsfoss.com/adobe-alternatives-linux/) (à noter que [`Gimp`](https://www.gimp.org) vient de scinder en 2 projets (gimp) et [`Glimpse`](https://www.omgubuntu.co.uk/2019/08/glimpse-gimp-image-editor-fork)).

A noter que par nature, les outils de création multimédia consomme pas mal d'énergie (mais pas autant que leur diffusion...).

##### Creation de posters

Plusieurs alternatives aux produits *Adobe* ou *Microsoft* existent. [`Latex`](https://fr.wikipedia.org/wiki/LaTeX), bien que complexe à appréhender, permet d'avoir un pouvoir d'expression élevé; si la syntaxe vous rebute, vous pouvez vous aider d'_[`overleaf`](https://www.overleaf.com/learn/latex/Posters)_. [`Scribus`](https://www.scribus.net/) et [`Inkscape`](https://inkscape.org/) sont 2 autres alternatives OpenSource dont l'utilisation sera beaucoup plus aisée.

#### Partage

Le streaming est, malheureusement, un des plus gros consommateurs d'énergie dans le monde de l'informatique ([article BBC](https://www.bbc.com/news/technology-45798523)), avec, entre autres, l'industrie du jeux vidéos et les cryptomonnaies.

Normalement, votre institution dispose d'un serveur de _podcast_. L'IN2P3 propose par exemple tout un système pour enregistrer un évènement en ligne ([webcast in2p3](https://webcast.in2p3.fr/)).
Si vous n'avez pas accès à ces facilités, vous pouvez utiliser [`Peertube`](https://joinpeertube.org/fr/). `PeerTube` est une alternative libre à `Youtube`, décentralisée et soutenue par [`Framasoft`](https://framasoft.org).
Comme `PeerTube` est décentralisé, il faudra utiliser un portail de recherche (comme [cet index](https://search.joinpeertube.org/)) qui cherchera sur un maximum d'instances.

### Mails

#### Mails privés

Ce n'est pas vraiment mon rôle de parler de cette problématique. Chacun est censé avoir un mail professionnel.
Ceci étant dit, il est important de séparer vie privée et vie professionnelle.

Dans ce cadre, je vous conseille d'éviter à nouveau les ogres du numérique (_google/gmail_, _microsoft/msn/hotmail_, ...), et de vous tourner vers d'autres fournisseurs de boîte mail, respectueux de votre vie privée, si possible, hébergés en France ou plus largement, En Europe (respect de la [`RGPD`](https://www.economie.gouv.fr/entreprises/reglement-general-sur-protection-des-donnees-rgpd)...). Nous pouvons, par exemple, citer le fournisseur de messagerie français [`mailo`](https://www.mailo.com/) (`mailvelope` ne semble pas fonctionnel avec ce dernier).

#### Client de messagerie

J'utilise [`Mozilla Thunderbird`](https://www.thunderbird.net) depuis de nombreuses années sans le moindre problème. Logiciel OpenSource affilié à la fondation Mozilla, il est capable d'utiliser des filtres très puissants, et de détecter les Spams/indésirables. Vous pouvez y adjoindre le plugin d'agenda [`lightning`](https://addons.thunderbird.net/fr/thunderbird/addon/lightning/) ainsi que de nombreux correcteurs d'orthographe.

#### Messagerie chiffree

[`ProtonMail`](https://protonmail.com/), développé à l'origine par le [CERN](https://home.cern/) fait figure de référence dans le domaine, mais il existe d'autres alternatives, telle que [`TutaNota`](https://tutanota.com/fr/).

Une liste intéressante de services de messagerie [ici](https://www.blog-libre.org/2020/05/01/messagerie-email-ethique-comment-preserver-sa-vie-privee-en-2020-et-celle-de-son-entourage/), vis à vis du respect de la vie privée.

#### Autres services de messageries

Deux autres solutions me paraissent intéressantes, car respectueuses de la vie privée :
  
  - [`posteo`](https://posteo.de/fr),
  - [`mailo`](https://www.mailo.com/fr/).

Le second a l'avantage d'être français et gratuit, même si le premier ne coûte qu'1€/mois. A noter que [`mailvelope`](https://www.mailvelope.com/en/) n'est pas encore compatible avec `mailo`.


### VPN

Normalement, vous disposez tous d'un VPN fournit par votre administration/entreprise hébergeante. Pour un VPN privé, je conseillerai l'utilisation d'une solution fait maison, telle que [`PiVPN`](http://www.pivpn.io/) (un VPN sur _raspberry Pi_), [`Damn Simple VPN`](https://github.com/jedisct1/dsvpn) (`DSVPN`) ou [`WireGuard`](https://www.wireguard.com/). L'installation pour `PiVPN` se fait très simplement. `DSVPN` permet d'utiliser des ports assez standards, peu/pas filtrés par les FAI/gouvernements.

Si vous n'avez pas la possibilité d'utiliser les solutions ci-dessus, il reste les services payants. Dans cet écosystème, je conseillerai plutôt la solution [`ProtonVPN`](https://protonvpn.com/), de la société [`ProtonMail`](https://protonmail.com/) issue du CERN. De nombreux articles décrivent et comparent les nombreux services de VPN disponibles (voir par exemple [ici](https://www.clubic.com/antivirus-securite-informatique/vpn/article-844220-1-comparatif-vpn.html)). Les principaux points à vérifier sont le prix, la vitesse ascendante et descendante dans le pays souhaité et qui fournit le service (à quoi ils s'engagent ?) et les serveurs (voir également leur localisation).

#### Tor

Certains connaissent sûrement [cette solution](https://www.torproject.org/) pour naviguer de manière privée/anonyme sur le Web. Je ne suis pas un fervent partisan de cette solution car le débit est souvent très fortement impacté (l'anonymat est fonction du chiffrement et du nombre de nœuds ou serveurs traversés, mais la vitesse est réduite statistiquement du fait de l'hétérogénéité du réseau). Par ailleurs, le chiffrement permettant de sécuriser les connexions n'est pas toujours garanti : le nœud d'entrée et le nœud de sortie pouvant potentiellement voir le contenu de ce qui transite. Ainsi, on constate que les nœuds se trouvent principalement en Iran, en Russie et aux États-Unis (cf [metrics](https://metrics.torproject.org/userstats-relay-table.html)).

Toutefois, `Tor` reste une solution peu coûteuse et très intéressante pour anonymiser ses connexions. La distribution [`Tails`](https://tails.boum.org/) embarque un navigateur `Tor` par défaut (ce dernier peut être téléchargé individuellement sur n'importe quelle plateforme). Il existe aussi un navigateur `Tor` pour smartphone. Le navigateur `Tor` est basé sur `Firefox`.

### Enregistrement et partage de publications

La référence commerciale est [`EndNote`](https://endnote.com). De mon point de vue, la meilleure solution est d'utiliser [`Zotero`](https://www.zotero.org/) avec les extensions pour [`Firefox`](https://www.zotero.org/download/) et [`LibreOffice`](https://extensions.libreoffice.org/extensions/zotero-libreoffice-integration).

### Redaction d'articles scientifiques

Oubliez [`MS Word`](https://products.office.com/fr-fr/word) ! Passez à **[Latex](https://www.latex-project.org/)** ! Oui, c'est plus facile à dire qu'à faire. Cependant, lorsque vous aurez testé une solution telle que [Overleaf](https://www.overleaf.com)/Sharelatex (dans le secteur public Enseignement Sup. Recherche, vous pouvez utiliser [`plmlatex`](https://plmlatex.math.cnrs.fr/login), un serveur [`sharelatex`](https://fr.sharelatex.com/) du réseau mathrice), vous ne pourrez plus vous en passer. En effet, ça permet d'avoir un aperçu en parallèle de la rédaction en _Latex_, et d'avoir une rédaction collaborative !

_Note HS_ : Pour la publication, pensez à passer par [des revues en OpenAccess](https://www.doaj.org/). Par ailleurs, pour votre expérience, il faudrait avoir des données librement consultables par tous (_OpenData_), avec des expériences reproductibles (cf. principe _FAIR_) dans le cadre d'un _"Data Management Plan"_ (_DMP_): https://ec.europa.eu/research/openscience .

### Suites bureautiques

Je ne suis pas un fervent défenseur d'une suite plutôt qu'une autre bien que certaines sont largement préférables à d'autres. En effet, dans un principe de longévité, de réutilisation/interopérabilité de la donnée et de versionnement (par exemple, avec **git**), je pense qu'il est largement préférable d'utiliser du texte enrichi (_[markdown](https://fr.wikipedia.org/wiki/Markdown)_, _[rst](http://docutils.sourceforge.net/docs/user/rst/quickref.html)_, _[asciidoc](https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/)_, voire éventuellement en _[wiki](https://fr.wikisource.org/wiki/Aide:La_syntaxe_wiki)_...), du **Latex** (bien que plus complexe à appréhender), des fichiers _csv_, etc... Ce fichier est d'ailleurs en _markdown_; vous pouvez en générer une version en HTML, en _.odt_ ou même en PDF avec le logiciel libre [**pandoc**](https://pandoc.org/).

Ceci étant dit, [`LibreOffice`](https://fr.libreoffice.org/) est largement préférable à `Microsoft Office`. Notamment pour le respect des standards et le référentiel général d'interopérabilité ([`RGI`](https://references.modernisation.gouv.fr/interoperabilite)). Ainsi, si quelqu'un vous envoie son CV en _`.odt`_, dans l'administration, vous n'avez pas le droit de le refuser à cause du format. De même, vous ne devriez pas échanger des fichiers _`docx`_ ou _`xlsx`_ mais _`odt`_ ou _`ods`_. De plus, `LibreOffice` est _OpenSource_.

#### Presentations, slides

Le logiciel [`MS PowerPoint`](https://products.office.com/fr-fr/powerpoint) est ici la référence. Cependant, avant de faire une présentation, il faut se demander qu'est-ce qui vous semble indispensable (?), quel public vous voulez toucher (?), sur quel support vous allez projeter (?) et si vous allez utiliser votre machine personnelle ou non (?). Tout le monde n'est pas forcément sensible aux animations (il est d'ailleurs connu que trop d'animations nuit au fond du discours). Si vous ne pouvez pas utiliser votre propre machine pour projeter, il y a de grandes chances pour que vous n'ayez droit qu'à un PDF. Selon le support, il faudra faire attention aux contrastes... Bref, on se rend assez vite compte que `PowerPoint` n'est pas la solution de présentation forcément indispensable. Son 1er concurrent, c'est [`Impress`](https://fr.libreoffice.org/discover/impress) de `libreoffice`; en de nombreux points, il est comparable à `PowerPoint`.

Nous pouvons aussi générer une présentation riche en animation et en graphisme avec du HTML/JS/css (les codes utilisés pour faire des sites Web). [`Prezi`](https://prezi.com/) en est d'ailleurs une référence; vous trouverez des alternatives OpenSource sur [cette page](https://alternativeto.net/software/prezi/?license=opensource). J'ai testé [`impress.js`](https://impress.js.org/) et [`reveal.js`](https://revealjs.com), et bien que ces 2 solutions soient assez impressionnantes, je reste, pour l'instant, peu convaincu. Je n'ai pas encore eu le loisir d'expérimenter correctement l'extension [`Sozi`](https://sozi.baierouge.fr/) pour [`Inkscape`](https://inkscape.org/), mais ça m'a l'air vraiment pas mal.

Si le superflu des animations présentes dans les présentations n'est pas pour vous, vous pouvez utiliser de nouveau `Latex` et [vous aider d'OverLeaf à nouveau](https://www.overleaf.com/learn/latex/Beamer). Cependant, vous remarquerez assez vite que la plupart des présentations en `Latex`, avec `beamer` se ressemblent.
Sachez donc qu'il est possible de [générer des présentations depuis un contenu en `markdown`](https://gist.github.com/johnloy/27dd124ad40e210e91c70dd1c24ac8c8). Cependant, je trouve que le nombre de styles/designs et de fonctionnalités sont encore un peu limités.

Sachez enfin qu'il est souvent possible de générer des présentations en _HTML/js/css_ ou _PDF_ depuis des outils ou langages tierces. C'est notamment le cas du package [`knitr`](https://cran.r-project.org/web/packages/knitr/index.html), à l'aide de [`Rmarkdown`](https://rmarkdown.rstudio.com/) et des [`HTML widgets`](https://cran.r-project.org/web/packages/htmlwidgets/index.html) en [`R`](https://www.r-project.org/).

#### Suites bureautiques en ligne, collaboratives

Toutefois, pour une écriture collaborative de document, il n'y a pour l'instant pas d'équivalent réel à [`MS Sharepoint`](https://products.office.com/fr-fr/sharepoint), [`Office 365`](https://www.office.com/) ou encore [`Google Docs`](https://docs.google.com). Il y a bien [`EtherCalc`](https://ethercalc.net/) avec notamment le service [`FramaCalc`](https://framacalc.org) pour le tableur, et [`CollaboraOnline`](https://nextcloud.com/collaboraonline/) en plugin de [`NextCloud`](https://nextcloud.com/), mais ça reste des solutions pour initiés. Il existe aussi la solution `OnlyOffice` ([dont le code est disponible sur GitHub](https://github.com/ONLYOFFICE/server)). Enfin, il y a [`CryptPad`](https://cryptpad.fr), qui m'a l'air assez prometteur, avec du chiffrement de bout en bout, OpenSource, mais encore un peu "jeune" pour être adopté pleinement, avec un stockage limité.

Ceci dit, je vous conseille d'essayer la solution [`Etherpad`](https://etherpad.org) avec le service [`Framapad`](https://framapad.org) ou celui de l'[_IN2P3_](https://etherpad.in2p3.fr/) et la solution [`EtherCalc`](https://ethercalc.org) avec le service [`FramaCalc`](https://framacalc.org).

## Reprenez le contrôle de votre vie privée

En dehors des applications chiffrées ou services anonymisant déjà évoquées, il existe quelques autres applications de contrôle ou paramétrages qui peuvent vous être utiles.


### Sur smartphone

[`fdroid`](https://f-droid.org/) permet d'installer des applications OpenSource sur votre smartphone Android. Il faut télécharger manuellement le fichier `apk` et l'installer.

[`Tasks.org`]( https://tasks.org/) : une application de gestion de tâche compatible avec [`nextcloud`](https://nextcloud.com/) avec [`davx5`](https://www.davx5.com/) pour la synchronisation.

<a id='exodus'></a>

[`Exodus privacy`](https://exodus-privacy.eu.org) : cette application pour smartphone vous permettra de connaître les pisteurs et autorisations données pour chaque application présente sur votre téléphone.

[`Shelter`](https://github.com/PeterCxy/Shelter), disponible sur [fdroid](https://f-droid.org/en/packages/net.typeblog.shelter/), permet d'isoler des application en faisant un clone de l'application dans un environnement isolé (`sandbox`). Utilisé en combinaison avec `Exodus privacy`, vous pourrez ainsi diminuer fortement les risques de diffusion de données personnelles sur votre smartphone. Lorsque l'application est clonée, et après avoir vérifié que le clone fonctionnait bien (bugs avec `Xiami` ?), vous pouvez désinstaller sans crainte la version originale.

[`Aurora Store`](https://gitlab.com/AuroraOSS/AuroraStore) est un client pour `Google play Store` qui vous permet d'installer des applications disponibles sur ce dernier, mais en tant qu'anonyme, sans besoin de se connecter à son compte Google. Vous pouvez installer Aurora Store en passant par [fdroid](https://f-droid.org/en/packages/com.aurora.store/).

**Blocage de pub** : le blocage de pub est une bonne mesure pour votre hygiène numérique. Non seulement, vous économiserez en bande passante et en temps cerveau en évitant de charger des choses inutiles, mais en plus, vous éviterez d'éventuels pisteurs supplémentaires.
[`Blokada DNS`](https://blokada.org/index.html) : cette application permet de bloquer originellement de la publicité par l'utilisation de [`DNS`](https://fr.wikipedia.org/wiki/Domain_Name_System) alternatifs. A noter qu'il vous permet aussi d'accéder parfois à des sites Web lorsque le DNS de votre FAI le bloque ou est en panne. [`DNSfilter`](https://sebsauvage.net/wiki/doku.php?id=dnsfilter) est une alternative à préférer à `Blokada DNS` si vous disposez de [`Fdroid`](https://f-droid.org/fr/), car plus légère. Voir également [`nebulo`](https://git.frostnerd.com/PublicAndroidApps/smokescreen) pour utiliser des DNS protégeant votre vie privée (rajoute aussi la possibilité d'utiliser un VPN simultanément).

Par ailleurs, si vous avez des comptes `Google`, `Facebook` (ou autres...), pensez à vérifier les autorisations que vous avez donné dans les paramètres de ces services en ligne. Des applications tierces sont parfois autorisées à accéder à votre contenu sur ces services ou faire des choses en votre nom...

Paramétrage de `firefox` : je vous conseille de paramétrer `Firefox` afin de limiter votre emprunte sur le Web ([lien firefox](https://support.mozilla.org/en-US/products/firefox/privacy-and-security)). _Edition_ -> _Préférences_ -> _Vie privée_. Il existe aussi [`Firefox Focus`](https://support.mozilla.org/fr/kb/firefox-focus) qui est un firefox dédié pour smartphone spécifique iOS qui, par défaut, est configuré pour limiter au maximum les traces que vous pouvez laisser sur Internet dans votre navigation.

Chez vous, vous pouvez aussi utiliser [**DoH**](https://www.bortzmeyer.org/8484.html) dans `Firefox` afin que votre requêtes DNS sur le Web soient elles-mêmes chiffrées ([lien zdnet](https://www.zdnet.fr/pratique/pratique-comment-activer-dns-over-https-doh-dans-firefox-39887257.htm)). Attention, toutefois, il faudra changer les serveurs DNS/DoH par défaut (`cloudflare`...), et vérifier leurs CGU ([cf. liste et outils](https://github.com/curl/curl/wiki/DNS-over-HTTPS))...

# Pointeurs, bibliographie

**Open... et libre**

- [OpenData](https://www.data.gouv.fr/),
- [OpenSource](https://fr.wikipedia.org/wiki/Open_source),
- [Free Software Foundation](https://www.fsf.org/),
- [La révolution Internet (version courte/vidéo peertube)](https://peertube2.cpy.re/videos/watch/22382fb9-ad79-4ac1-8df3-c7c027749425;threadId=52996),
- [Licences OpenSource](https://opensource.org/licenses),
- [April](https://www.april.org/),
- [La neutralité du Net sur la Quadrature du Net](https://www.laquadrature.net/neutralite_du_Net/),
- [Socle Interministériel de Logiciels Libres (SILL)](http://references.modernisation.gouv.fr/socle-logiciels-libres).

**Hébergeurs de services utiles**

- [Renater](https://www.renater.fr/),
- [Framasoft](https://framasoft.org/fr/),
- [IndieHosters](https://indie.host),
- [Collectif CHATONS](https://chatons.org/).

**Ecologie et informatique**

- [ecoinfo](https://ecoinfo.cnrs.fr/),
- [greenIT](https://www.greenit.fr/),
- [green500](https://www.top500.org/green500/),
- [matinfo4 ecoinfo](https://www.matinfo-esr.fr/ecoinfo),
- [L'empreinte carbone du numérique](https://www.arcep.fr/uploads/tx_gspublication/reseaux-du-futur-empreinte-carbone-numerique-juillet2019.pdf) 07/2019, par l'ARCEP,
- [The Shift project / Pour une sobriété numérique](https://theshiftproject.org/article/pour-une-sobriete-numerique-rapport-shift/),
- [article de greenpeace](https://www.greenpeace.fr/il-est-temps-de-renouveler-internet/),
- [Les problèmes environnementaux de la terre
d’innovation, Marc Chatreux, JRES 2019 (PDF)](https://conf-ng.jres.org/2019/document_revision_5432.html?download)
- [Consommation réseau/Streaming](https://fortune.com/2018/10/02/netflix-consumes-15-percent-of-global-internet-bandwidth/),
- Articles de blog :
    - [Les politiques publiques doivent impulser un numérique social et écologique](https://cdonnees.com/2019/09/05/les-politiques-publiques-doivent-impulser-un-numerique-social-et-ecologique/),
    - [L'impact écologique du numérique](https://blog.dreads-unlock.fr/limpact-ecologique-du-numerique/),
    - [Supprimer la publicité en ligne pour un numérique plus écologique ?](https://holory.fr/supprimer-la-publicite-en-ligne-pour-un-numerique-plus-ecologique/),
    - [Numérique et écologie : les data centers, des gouffres énergétiques ?](https://www.sciencesetavenir.fr/high-tech/informatique/numerique-et-ecologie-les-data-centers-des-gouffres-energetiques_121838).

**Interopérabilité**

- [Référentiel général d'interopérabilité, RGI](https://references.modernisation.gouv.fr/interoperabilite).

**Comparaisons / listings de logiciels**

- [switching.social](https://switching.social/),
- [Projet plume / Fénix](https://www.projet-plume.org/),
- [Framalibre](https://framalibre.org/annuaires/logiciel),
- [alternativeTo](https://alternativeto.net/),
- [slant](https://www.slant.co).

**Vie privée**

- [Moteurs de recherche](https://www.clubic.com/guide-achat/article-843987-1-google-qwant-duckduckgo-comparatif-6-moteurs-recherche.html),
- [Dégooglisons Internet](https://degooglisons-internet.org/),
- [How I fully quit google](https://medium.com/s/story/how-i-fully-quit-google-and-you-can-too-4c2f3f85793a),
- [Communiquer vers/depuis la Chine](https://blog.saimyx.site//?d=2019/09/07/13/14/53-communiquer-vers-et-depuis-la-chine),
- [RGPD](https://www.economie.gouv.fr/entreprises/reglement-general-sur-protection-des-donnees-rgpd),
- [CNIL : maîtriser mes données](https://www.cnil.fr/fr/maitriser-mes-donnees),
- [Vie privée et intimité numérique](https://framablog.org/2018/10/23/intimite-numerique-le-truc-a-fait-mouche/),
- [Conférence devant lycéens - Nothing to hide ? - Julien Vaubourg (vidéo)](https://troll.tv/videos/watch/14eedbab-89d1-4a52-b3ee-976b04db473f),
- [Pourquoi et comment j'ai quitté les GAFAM](https://net-security.fr/security/privacy/pourquoi-et-comment-jai-quitte-les-gafam/),
- [Why I quit google](https://www.kylepiira.com/2020/01/09/why-i-quit-google/),
- Nowtech (Youtube) : 
    - [Android sans Google : mon avis après 1 an](https://www.youtube.com/watch?v=Bb5Kh9DryvY),
    - [Comment supprimer Google de son smartphone](https://www.youtube.com/watch?v=V0YNTJu3raw).
<!--- [Film documentaire "Nothing to hide" (vidéo)](https://tube.benzo.online/videos/watch/d0f08027-2849-42d3-b631-3fd83362fc2f)-->
<!--- [The Great Hack : L'affaire Cambridge analytica (vidéo)](https://tube.benzo.online/videos/watch/9b99fc95-b622-4c89-afc5-cb8b5bb0ed36)-->
- [blog dreads-unlock : Les bons réflexes](https://blog.dreads-unlock.fr/vers-une-hygiene-numerique-les-bons-reflexes/),
- [blog dreads-unlock : Hygiène numérique avec android](https://blog.dreads-unlock.fr/ameliorer-son-hygiene-numerique-sur-android/),
- [site e-traces](https://etraces.constantvzw.org/informations/).


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
